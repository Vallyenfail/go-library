package controllers

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/Vallyenfail/go-library/internal/service"
	"net/http"
	"strconv"
	"time"
)

type Controllers interface {
	AuthorsGetListRequest() gin.HandlerFunc
	AuthorsTopListRequest() gin.HandlerFunc
	AuthorAddRequest() gin.HandlerFunc
	GetBookByUserRequest() gin.HandlerFunc
	BooksListRequest() gin.HandlerFunc
	BookAddRequest() gin.HandlerFunc
	ReturnBookRequest() gin.HandlerFunc
	UsersListRequest() gin.HandlerFunc
}

type Controller struct {
	service service.Service
}

func NewController(service service.Service) *Controller {
	return &Controller{service: service}
}

func (c *Controller) AuthorsGetListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		authors, err := c.service.GetListAuthors(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, authors)
	}
}

func (c *Controller) AuthorsTopListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		authors, err := c.service.GetTopAuthors(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, authors)
	}
}

func (c *Controller) AuthorAddRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		authorName := g.PostForm("name")

		if err := c.service.AddAuthor(ctx, authorName); err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

func (c *Controller) GetBookByUserRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		uId := g.Param("user_id")
		bId := g.Param("book_id")

		bookId, err := strconv.Atoi(bId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}
		userId, err := strconv.Atoi(uId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		if err = c.service.GetBookByUser(ctx, bookId, userId); err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

func (c *Controller) BooksListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		books, err := c.service.BooksList(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, books)
	}
}

func (c *Controller) BookAddRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		aId := g.Param("AuthorId")
		authorId, err := strconv.Atoi(aId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		title := g.PostForm("title")

		err = c.service.BookAdd(ctx, authorId, title)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

func (c *Controller) ReturnBookRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		bId := g.Param("book_id")
		uId := g.Param("user_id")

		bookId, err := strconv.Atoi(bId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}
		userId, err := strconv.Atoi(uId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		err = c.service.ReturnBook(ctx, bookId, userId)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.Writer.WriteHeader(http.StatusOK)
	}
}

func (c *Controller) UsersListRequest() gin.HandlerFunc {
	return func(g *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		users, err := c.service.UsersList(ctx)
		if err != nil {
			g.JSON(http.StatusBadRequest, err.Error())
			return
		}

		g.JSON(http.StatusOK, users)
	}
}
