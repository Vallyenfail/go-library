package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Vallyenfail/go-library/controllers"
)

type Router struct {
	router controllers.Controllers
}

func NewRouter(controller controllers.Controllers) *Router {
	return &Router{router: controller}
}

func (r *Router) Route(router *gin.Engine) {
	router.GET("/authors", r.router.AuthorsGetListRequest())
	router.GET("/authors/top", r.router.AuthorsTopListRequest())
	router.POST("/authors", r.router.AuthorAddRequest())
	router.PUT("/books/takes/:book_id/users/:user_id", r.router.GetBookByUserRequest())
	router.PUT("/books/return/:book_id/users/:user_id", r.router.ReturnBookRequest())
	router.POST("/books/:AuthorId", r.router.BookAddRequest())
	router.GET("/users", r.router.UsersListRequest())
	router.GET("/books", r.router.BooksListRequest())
}

func (r *Router) Swagger(router *gin.Engine) {
	router.GET("/swagger/*any", swaggerUI)
}
