package service

import (
	"context"
	"gitlab.com/Vallyenfail/go-library/internal/models"
	"gitlab.com/Vallyenfail/go-library/internal/repository"
)

type Service interface {
	GetListAuthors(ctx context.Context) ([]models.Author, error)
	AddAuthor(ctx context.Context, name string) error
	GetTopAuthors(ctx context.Context) ([]models.Author, error)
	GetBookByUser(ctx context.Context, bookId int, userId int) error
	BooksList(ctx context.Context) ([]models.Book, error)
	BookAdd(ctx context.Context, authorId int, title string) error
	ReturnBook(ctx context.Context, bookId, userId int) error
	UsersList(ctx context.Context) ([]models.User, error)
}

type Serv struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *Serv {
	return &Serv{repository: repository}
}

func (s *Serv) GetListAuthors(ctx context.Context) ([]models.Author, error) {
	return s.repository.GetListAuthors(ctx)
}

func (s *Serv) GetTopAuthors(ctx context.Context) ([]models.Author, error) {
	return s.repository.GetTopAuthors(ctx)
}

func (s *Serv) AddAuthor(ctx context.Context, name string) error {
	return s.repository.AddAuthor(ctx, name)
}

func (s *Serv) GetBookByUser(ctx context.Context, bookId int, userId int) error {
	return s.repository.GetBookByUser(ctx, bookId, userId)
}

func (s *Serv) BooksList(ctx context.Context) ([]models.Book, error) {
	return s.repository.BooksList(ctx)
}

func (s *Serv) BookAdd(ctx context.Context, authorId int, title string) error {
	return s.repository.BookAdd(ctx, authorId, title)
}

func (s *Serv) ReturnBook(ctx context.Context, bookId, userId int) error {
	return s.repository.ReturnBook(ctx, bookId, userId)
}

func (s *Serv) UsersList(ctx context.Context) ([]models.User, error) {
	return s.repository.UsersList(ctx)
}
