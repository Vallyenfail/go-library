package models

import (
	"database/sql"
)

type Book struct {
	ID         int            `json:"id" db:"id"`
	Author     *Author        `json:"author"`
	AuthorID   sql.NullInt64  `db:"author_id"`
	AuthorName sql.NullString `db:"author_name"`
	Title      string         `json:"title" db:"title"`
	User       *User          `json:"user"`
	UserID     sql.NullInt64  `db:"user_id"`
	UserName   sql.NullString `db:"user_name"`
}
