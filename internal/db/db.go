package db

import (
	"errors"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/icrowley/fake"
	"github.com/jmoiron/sqlx"
	"log"
	"os"
)

func NewDB() (*sqlx.DB, error) {
	var dsn string
	var DBRaw *sqlx.DB

	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	dsn = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPassword, dbName)
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		return nil, errors.New("cannot open db")
	}
	if err = db.Ping(); err != nil {
		log.Println(err.Error())
		return nil, err
	}

	migrationsPath := "file://internal/migrations"

	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		log.Println(err)
		return nil, err
	}

	m, err := migrate.NewWithDatabaseInstance(migrationsPath, "postgres", driver)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	err = m.Up()
	if err != nil {
		if err == migrate.ErrNoChange {
			log.Println("No migrations to apply")
		} else {
			log.Println(err)
			return nil, err
		}
	}

	var count int

	if err := db.QueryRow("SELECT COUNT(*) FROM authors").Scan(&count); err != nil {
		panic(err)
	}

	if count == 0 {
		for i := 0; i < 10; i++ {
			author := gofakeit.Name()
			if _, err := db.Exec("INSERT INTO authors (name) VALUES ($1)", author); err != nil {
				panic(err)
			}
		}
	}

	if err := db.QueryRow("SELECT COUNT(*) FROM books").Scan(&count); err != nil {
		panic(err)
	}

	if count == 0 {
		for i := 0; i < 100; i++ {
			var authorID int
			if err := db.QueryRow("SELECT id FROM authors ORDER BY random() LIMIT 1").Scan(&authorID); err != nil {
				panic(err)
			}

			book := gofakeit.BeerName()
			if _, err := db.Exec("INSERT INTO books (title, author_id) VALUES ($1, $2)", book, authorID); err != nil {
				panic(err)
			}
		}
	}

	if err := db.QueryRow("SELECT COUNT(*) FROM users").Scan(&count); err != nil {
		panic(err)
	}

	if count == 0 {
		for i := 0; i < 50; i++ {
			name := fake.FirstName()
			if _, err := db.Exec("INSERT INTO users (name) VALUES ($1)", name); err != nil {
				panic(err)
			}
		}
	}

	log.Println("Migrations applied successfully")
	DBRaw = db

	return DBRaw, nil
}
