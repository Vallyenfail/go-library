#Build stage
FROM golang:1.19-alpine AS builder

WORKDIR /public
#Copy everything in the vacancy_parser to the working directory in the docker
COPY . .
#Build the file which has the name vacancy_parser
RUN go build -o library ./cmd/api/main.go

#Run stage
FROM alpine:3.13

WORKDIR /public
COPY --from=builder  /public/library .
#Which port container listens to
#EXPOSE 8080
COPY . .
#The path to the executable file in the container
CMD ["/public/library"]