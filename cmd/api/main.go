package main

import (
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/Vallyenfail/go-library/controllers"
	"gitlab.com/Vallyenfail/go-library/internal/db"
	"gitlab.com/Vallyenfail/go-library/internal/repository"
	"gitlab.com/Vallyenfail/go-library/internal/router"
	"gitlab.com/Vallyenfail/go-library/internal/service"
	"log"
	"net/http"
	"os"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Cannot load .env file")
	}
}

func main() {
	routers := gin.Default()

	//mainRoute := routers.Group("/")

	dataBase, err := db.NewDB()
	if err != nil {
		log.Println(err)
	}

	r := repository.NewRepository(dataBase)
	s := service.NewService(r)
	c := controllers.NewController(s)
	h := router.NewRouter(c)
	h.Route(routers)

	h.Swagger(routers)
	routers.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	address := os.Getenv("API_ADDRESS")
	routers.Run(address)
}
